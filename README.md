# Garmin Connect routine analyser

This program analyses my fitness activites from Garmin Connect, It requires I set my activites as 'Strength Training' and name them in the following format

`3/3 + Stretches` = I did 3 out of 3 sets and a set of stretches.
`1/3` =  I did 1 out of 3 sets and no stretches

## Requirements

Tested with:
*  Python 3.8.2
*  numpy
*  pandas

## Goals

I want this program to output graphs so I can see how consistently I'm doing my set exercises and if I don't do all sets.

## ToDo

*  Workout how to output graphs and calendar heatmaps
*  Decide what graphs/parameters are important & useful
*  Add python requirements.txt