#!/usr/bin/env python
import pandas as pd
import numpy as np

# Config
input_path = "input.csv"
date_limit = "2020-02-22"
to_drop = [
    'Favorite', 'Distance', 'Calories',
    'Avg Run Cadence', 'Max Run Cadence', 'Avg Speed',
    'Max Speed', 'Avg Stride Length', 'Avg Vertical Ratio',
    'Avg Vertical Oscillation', 'Training Stress Score®', 'Grit',
    'Flow', 'Bottom Time', 'Min Temp',
    'Surface Interval', 'Decompression', 'Best Lap Time',
    'Number of Laps', 'Max Temp'
    ]

def makeCSV(df):
    df.to_csv('output.csv', sep=',', encoding='utf-8')

def showAll(df):
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
        print(df)

def cleanData(df):
    df.set_index('Date', inplace=True) # Set the date as the unique key
    df = df.loc[:date_limit] # Drop rows older than dateLimit
    df.drop(to_drop, inplace=True, axis=1) # Drop columns
    df = df[~df['Activity Type'].isin(['Cycling', 'Indoor Cycling', 'Walking'])] # Drop non-routine exercises
    df = df.replace('--', np.nan) # Replace '--' with -1
    df['Title'] = df['Title'].str.lower()

    # Insert custom columns
    df['Stretches'] = df['Title'].str.contains('stretches')
    df['Routine Sets'], df['Routine Max'] = df['Title'].str.split('/', 1).str
    df['Routine Sets'] = df['Routine Sets'].replace('stretches', np.nan)
    df['Routine Max'] = df['Routine Max'].str[:1]

    # Change column types
    df['Avg HR'] = df['Avg HR'].astype(float)
    df['Max HR'] = df['Max HR'].astype(float)
    df['Total Reps'] = df['Total Reps'].astype(float)
    df['Total Sets'] = df['Total Sets'].astype(float)
    df['Routine Sets'] = df['Routine Sets'].astype(float)
    df['Routine Max'] = df['Routine Max'].astype(float)

    return df

def graph(df):
    import matplotlib.pyplot as plt
    
    x, y = ['Avg HR', 'Time']
    
    df = pd.DataFrame(df,columns=[y,x])
    df.plot(x=x, y=y, kind='scatter')
    plt.show()


if __name__ == '__main__':
    df = pd.read_csv(input_path, sep=',', date_parser=pd.to_datetime) # We read the CSV, then limit our scope.
    df = cleanData(df)
    graph(df)
